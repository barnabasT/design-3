//===============================LIBRARY IMPORTS======================================
  #include <ESP8266WiFi.h>
  #include <ESP8266mDNS.h>
  #include <EEPROM.h>
  #include <DNSServer.h>
  #include <ESP8266WebServer.h>
  #include <WiFiManager.h> 
  #include <string.h>
  #include <FS.h>
  #include "OneButton.h"  
//==================================CONSTANTS=========================================
  #define CONFIG "/config.txt"
  #define LOGS "/logs.txt"
  #define BUTTON_PIN D5

//==================================VARIABLES=========================================
  WiFiServer  Server(443);      
  WiFiClient  Client;    
  WiFiManager wifiManager;
  
  OneButton button(BUTTON_PIN,true);
  
  // Network Name and Password
  char*       net_ssid = "";              // WIFI NAME
  char*       net_pass = "";          // PASSWORD  
  
  String HubID;
  String AuthKey;
  String AccessRecords;
  String Type;
  String Room;
  String AccessLevel;
  String Config;
  String AccKeys;
  String Owner;
  String isSetup;
  String first;
  
  int Value;
  int address;
  int over=0,admin = 0;

  bool    spiffsActive = false;
  bool isSetUp;
 

//===================================FUNCTIONS============================================

//--------------------------------Change-WiFi-Mode----------------------------------------
  void setWiFiMode(){
    
    WiFiManager wifiManager1;
    if (!wifiManager1.startConfigPortal("ACES-Portal")) {
      Serial.println("failed to connect and hit timeout");
      delay(3000);
      //reset and try again, or maybe put it to deep sleep
            
      ESP.reset();
      delay(5000);
    }
    //if you get here you have connected to the WiFi
    Serial.println("Connected to CLIENT:)");
    writeToFirst(1, 4000);
    Serial.println("local ip");
    Serial.println(WiFi.localIP());
        
  }
//--------------------------------Set-Access-Point----------------------------------------
  void SetWifi(char* Name, char* Password)
  {
    // Stop Any Previous WIFI
    WiFi.disconnect();

    // Setting The Wifi Mode
    WiFi.mode(WIFI_AP_STA);
    Serial.println("WIFI Mode : AccessPoint Station");
    
    // Setting The AccessPoint Name & Password
    net_ssid      = Name;
    net_pass  = Password;
    
    // Starting The Access Point
    WiFi.softAP(net_ssid, net_pass);
    Serial.println("WIFI << " + String(net_ssid) + " >> has Started");
    
    // Wait For Few Seconds
    delay(2000);
    
    // Getting Server IP
    IPAddress IP = WiFi.softAPIP();
    
    // Printing The Server IP Address
    Serial.print("Server IP : ");
    Serial.println(IP);

    // Starting Server
    Server.begin();
    Server.setNoDelay(true);
    Serial.println("Server Started, you can connect from the JAVA client");

    //Variables
    checkFirstAddr();  
  }  
//--------------------------------Available-Client----------------------------------------
  void AvailableClients()
  {   
    
    if (Server.hasClient())
    {              
          // Checks If Clients Connected To The Server
          if(Client = Server.available()){
            Serial.println("New Client discovered");
            Client.println("Config");              
            Client.println(HubID + "\n" + AuthKey  + "\n" + AccessRecords + "\n" + Type + "\n" + Room + "\n" + AccessLevel + "\n" + "Config," +Config + "\n" + AccKeys + "\n" + Owner);       
          }      
      //no free/disconnected spot so reject
      WiFiClient Client = Server.available();
      Client.stop();
    }
    
  }
//--------------------------------Available-Message---------------------------------------
void AvailableMessage()
  {
    //check clients for data
      if (Client && Client.connected() && Client.available()) 
      {
          while(Client.available()) //while(Client[i].available())
          {
            String Message = Client.readStringUntil('\n');            
            Message.trim();
            int idxUser = Message.indexOf("?");
            String User = Message.substring(0,idxUser--);
            Serial.println("message received");
            Serial.println(Message);
            
            if (User.equals("Admin")){
              Serial.println("Inside the admin user conf");
              //int idxSet = Message.indexOf("#");
              int idxHID = Message.indexOf("*");// returns the position of symbol*
              int idxAuthKey = Message.indexOf("~");
              int idxAuthUsers = Message.indexOf(".");
              int idxType = Message.indexOf(",");
              int idxRoomNo = Message.indexOf("`");
              int idxAccessLevel = Message.indexOf("@");
              //int idxConfig = Message.indexOf("$");
              int idxOwner = Message.indexOf("%");

              Serial.println("Now Splittig String");
              
              HubID = Message.substring(idxUser+2,idxHID--);
              AuthKey = Message.substring(idxHID+2,idxAuthKey--);
              AccessRecords = Message.substring(idxAuthKey+2,idxAuthUsers--);
              Type = Message.substring(idxAuthUsers+2,idxType--);
              Room = Message.substring(idxType+2,idxRoomNo--);
              AccessLevel = Message.substring(idxRoomNo+2,idxAccessLevel--);
             // Config = Message.substring(idxAccessLevel+2,idxConfig--);              
              Owner = Message.substring(idxAccessLevel+3,idxOwner--);      

              writeToFirst(1,0);
              Serial.println(HubID);
              Serial.println(AuthKey);
              Serial.println(AccessRecords);
              Serial.println(Type);
              Serial.println(Room);
              Serial.println(AccessLevel);
              Serial.println(Config);
              Serial.println(Owner);
              Serial.println(isSetUp);    
            }else if (User.equals("Normal")){              
              int idxConfig = Message.indexOf("$");
              int idxAuthUsers = Message.indexOf("%");
              
              Serial.println("Inside the normal user conf");              
              Config = Message.substring(idxUser+2,idxConfig--); 
              AccessRecords = Message.substring(idxConfig+2,idxAuthUsers--);             
              Serial.println(Config);
              Serial.println(AccessRecords);
            }else if (User.equals("AdminUser")){
              int idxConfig = Message.indexOf("$");
              Config = Message.substring(idxUser+2,idxConfig--);                            
            }
            setGlobals();
            Client.flush();             
          }
      }
      over=0;
    
  }  
//-------------------------------------EEPROM---------------------------------------------

  void amIsetUp(){
    address = 0;
    EEPROM.begin(4096);
    String isSetup = readfromee(address);
    if (isSetup == "1"){
      isSetUp = true;
      Serial.println("Is set UP");
    }else{
      isSetUp = false;
      Serial.println("Is not set UP");
    }
    EEPROM.end();
  }
  
  void getGlobals(){
    address = 1;
    EEPROM.begin(4096);
    HubID = readfromee(address);    
    AuthKey = readfromee(address);    
    AccessRecords = readfromee(address);    
    Type = readfromee(address);    
    Room = readfromee(address);    
    AccessLevel = readfromee(address);    
    Config = readfromee(address);    
    AccKeys = readfromee(address);    
    Owner = readfromee(address);    
    EEPROM.end();
  }
  
  void setGlobals(){
    address = 1;
    EEPROM.begin(4096);
    writetoee(HubID, address);
    writetoee("%", address);
    writetoee(AuthKey, address);
    writetoee("%", address);
    writetoee(AccessRecords, address);
    writetoee("%", address);
    writetoee(Type, address);
    writetoee("%", address);
    writetoee(Room, address);
    writetoee("%", address);
    writetoee(AccessLevel, address);
    writetoee("%", address);
    writetoee(Config, address);
    writetoee("%", address);
    writetoee(AccKeys, address);
    writetoee("%", address);
    writetoee(Owner, address);
    writetoee("%", address);
    Serial.println("Done Setting Globals");
    EEPROM.end();
  }
  
  void writetoee(String string, int &addr){
    int i = 0;
    //Serial.println("Start Writing");
    //Serial.println("Address: "+addr);
    EEPROM.begin(4096);
    for (i; i < string.length(); i++){
      //Serial.println(string[i]);
      EEPROM.write(addr++, string[i]);
      delay(100);
      //Serial.print("Addr");           
      //Serial.println(addr-1);
      //Serial.print("read:"); 
      //Serial.println(char(EEPROM.read(addr-1)));
      Serial.print("");
    }
    //addr = addr + i;
    EEPROM.commit();  
    EEPROM.end();
    Serial.println("Done Writing");
  }
  
  String readfromee(int &addr){
    
    char value;
    String Result;
    for (addr; EEPROM.read(addr) != '%'; addr++){
      value = EEPROM.read(addr);
      Result += value;
      //delay(500);
    }
    //Serial.println("Address");
    //Serial.println(addr-1);
    //Serial.println(Result);
    addr++;    
    return Result;
  }

  void writeToFirst(int value, int address) {
    EEPROM.begin(4096);
    EEPROM.write(address, value);  
    EEPROM.commit();                
    Value = (EEPROM.read(0));
    if (Value == 1) {      
      Serial.println("The Value Written is:");
      Serial.println(Value);
    }
    EEPROM.end();
  }
  void checkFirstAddr() {
    EEPROM.begin(4096);               
    Value = (EEPROM.read(0));
    if (Value == 1) {      
      Serial.println("The Value Read is:");
      Serial.println(Value);  
      //getGlobals();
    }
    EEPROM.end();
  }
//----------------------------------Button Press------------------------------------------
  void doubleclick() {                                // what happens when button is double-clicked
   Serial.println("Double Click");
   WiFi.disconnect(true);
   writeToFirst(0, 4000);
   ESP.reset();
  }
 
  void singleclick(){                                 // what happens when the button is clicked
    Serial.println("Single Click");    
    WiFi.disconnect(true);
    delay(300);

    //check andre's bit
    setWiFiMode(); 
    delay(5000);
  }
  
  /*void buttonPressed()
  {
    Serial.println("ButtonPressed");
    int button = digitalRead(BUTTON_PIN);
    if(button == HIGH)
    {
      Serial.println("Pin is High");
      //wifiManager.resetSettings();
      WiFi.disconnect(true);
      
      //ESP.reset();
      delay(5000);
    }
    return;
  }*/


//===================================SETUP============================================

  void setup(){
    //interrupt settings
    pinMode(BUTTON_PIN, INPUT);
    //attachInterrupt(BUTTON_PIN, buttonPressed, RISING);  
    button.attachDoubleClick(doubleclick);            // link the function to be called on a doubleclick event.
    button.attachClick(singleclick);                  // link the function to be called on a singleclick event.
//    button.attachLongPressStop(longclick);            // link the function to be called on a longpress event
  
    // Setting Serial Port
    Serial.begin(4800);           // Computer Communication

    // Setting Wifi Access Point
    SetWifi("ACES", "");  
    //setWiFiMode(); 

    // SPIFF setup
    delay(1000);
    
    //attachInterrupt(12,ISR,RISING);
    
    // Start filing subsystem
    if (SPIFFS.begin()) {
        Serial.println("SPIFFS Active");        
        Serial.println();
        spiffsActive = true;
    } else {
        Serial.println("Unable to activate SPIFFS");
    }
    delay(2000);
    getGlobals();
    delay(1000);
  }

//==============================MAIN-LOOP=============================================
  
  void loop(){

    button.tick();

    delay(100);

    EEPROM.begin(4096);               
    Value = (EEPROM.read(4000));
    EEPROM.end();
    Serial.println("Current Mode:");
    Serial.println(Value);  
    if (Value == 1) {      
      Serial.println("Barnabas Loop");       
      AvailableClients();    
      AvailableMessage();  
    }else{
      Serial.println("Andre's Loop");
    } 
      
    
    
    /*int button = digitalRead(BUTTON_PIN);
    if (button == HIGH)
    {
      Serial.println("Pin is High");
      //wifiManager.resetSettings();
      WiFi.disconnect(true);
      setWiFiMode(); 
      //ESP.reset();
      delay(5000);
    }*/
  }
//====================================================================================
