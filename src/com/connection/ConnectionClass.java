package com.connection;

import com.aces.Main;
import javafx.scene.control.Alert;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionClass {
    public Connection connection;
    public  Connection getConnection(){


        String dbName="aces_db";
        String userName="root";
        String password="";

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();

            //connection= DriverManager.getConnection("jdbc:mysql://146.230.192.204/"+dbName,userName,password);
            connection= DriverManager.getConnection("jdbc:mysql://localhost/"+dbName,userName,password);


        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Connection error");
            alert.setHeaderText(null);
            alert.setContentText("There is no communication with the database server");
            alert.showAndWait();
            Main.closeProgram();
        }


        return connection;
    }

}