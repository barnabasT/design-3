package com.aces;

import com.connection.ConnectionClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.PrinterJob;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.*;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.acl.Owner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

public class MainController implements Initializable, ControlledScreen {
    @FXML
    public ListView listAuthUsers;
    @FXML
    public TextArea txtLogs;
    @FXML
    public ToggleButton btnDallas;
    @FXML
    public ToggleButton btnNFC;

    private static final String USER_AGENT = "Mozilla/5.0";
    public MenuItem normalLogout;


    private String owner, authUsers, selected;
    public String dallas,nfcCard;
    private String[] strs;
    private PrinterJob job;
    Stage window;

    ScreensController myController;
    Controller c = new Controller();

    ConnectionClass connectionClass = new ConnectionClass();
    Connection connection = connectionClass.getConnection();
    public PreparedStatement pst;
    public ResultSet rs;
    static String fileName= "";

    final ObservableList options = FXCollections.observableArrayList();

    FTPClient ftp = new FTPClient();

    //String serverAddress="146.230.192.204";
    String serverAddress="127.0.0.1";
    String userId="user",password="aces";



    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        System.out.println("Loading main controller");
    }


    public void populateList(String user) {
        this.owner = user;
        listAuthUsers.setItems(options);


        try {
            System.out.println(user);
            String sql = "SELECT Users FROM hubs WHERE Owner = '" + owner + "';";
            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                authUsers = rs.getString("Users");
                System.out.println("Before Split");
                System.out.println(authUsers);
                char delim = '^';
                String[] strs = authUsers.split("\\^");
                System.out.println("After Split");

                for (String str : strs) {
                    System.out.println(str);
                    options.add(str);
                }
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }


    public void addAuth(ActionEvent actionEvent) {
        TextInputDialog dialog = new TextInputDialog("Student or Staff Number");
        dialog.setTitle("Add User");
        dialog.setHeaderText(null);
        dialog.setContentText("Staff/Student Number:");

        Optional<String> result = dialog.showAndWait();

        if(result.isPresent()){
            authUsers = authUsers + result.get()+ "^";
            options.add(result.get());
            updateDB();
        }

    }

    public void delAuth(ActionEvent actionEvent) {
        authUsers = authUsers.replace((selected + ";"), "");
        options.remove(selected);
        updateDB();
    }

    public void updateDB(){
        try {
            String sql = "UPDATE hubs SET Users=? WHERE Owner= '" + owner + "';";
            pst = connection.prepareStatement(sql);
            pst.setString(1, authUsers);
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void selectList(MouseEvent mouseEvent) {
        selected = (String)listAuthUsers.getSelectionModel().getSelectedItem();

        try {
            String sql = "SELECT Users FROM hubs WHERE Owner = ?";
            pst = connection.prepareStatement(sql);
            pst.setString(1, selected);
            rs = pst.executeQuery();

            while (rs.next()) {
                txtLogs.setText(rs.getString("Users"));
            }
            pst.close();
            rs.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void exit(ActionEvent actionEvent) {
        Main.closeProgram();
    }

    public void handleDallas(ActionEvent actionEvent) {
        if(btnDallas.isSelected()){
            btnDallas.setStyle("-fx-base:  #232930;-fx-text-fill: white;");
            dallas = "1";
        }else{
            btnDallas.setStyle("-fx-base:  #F3851E;");
            dallas = "0";
        }

    }

    public void handleNFC(ActionEvent actionEvent) {
        if(btnNFC.isSelected()){
            btnNFC.setStyle("-fx-base:  #232930;-fx-text-fill: white;");
            nfcCard = "1";
        }else{
            btnNFC.setStyle("-fx-base: #F3851E;");
            nfcCard = "0";
        }
    }

    public void handleReadLogs(ActionEvent actionEvent) {
        String room="";
        try {

            String sql = "SELECT Room FROM hubs WHERE Owner = '" + owner + "';";
            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                room = rs.getString("Room");
                System.out.println(room);
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        try{



            //try to connect
            ftp.connect(serverAddress);

            //login to server
            if(!ftp.login(userId, password))
            {
                ftp.logout();
                System.out.println("Login Error");
            }else{
                System.out.println("Login Success");
            }

            int reply = ftp.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes.
            if (!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                System.out.println("Connection Error");
            }else{
                System.out.println("Successful Connection");
            }

            //enter passive mode
            ftp.enterLocalPassiveMode();
            //get system name
          //  System.out.println("Remote system is " + ftp.getSystemType());
            //get current directory
            System.out.println("Current directory is " + ftp.printWorkingDirectory());

            //get list of filenames
            FTPFile[] ftpFiles = ftp.listFiles();


            GetDirFile(ftpFiles);


            //change current directory
            ftp.changeWorkingDirectory(room);
            System.out.println("Current directory is " + ftp.printWorkingDirectory());
            //get list of filenames
            ftpFiles = ftp.listFiles();
            GetDirFile(ftpFiles);

            System.out.println("Uploaded File Content\n[");





            //Getting the File in an InputStream
            File localFile=new File(fileName);
            InputStream remoteInput=ftp.retrieveFileStream(localFile.getName());
            BufferedReader in = new BufferedReader(new InputStreamReader(remoteInput));
            String line="";
            String log="";
            while((line = in.readLine()) != null) {
                System.out.println(line);
                log = log + line + "\n";
            }
            txtLogs.setText(log);

            System.out.println("]");

            remoteInput.close();

            ftp.logout();
            ftp.disconnect();

        }catch(IOException ex){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Connection Error");
            alert.setHeaderText(null);
            alert.setContentText("Ensure that FTP Server is online.");
            alert.showAndWait();
        }


    }

    private static void GetDirFile(FTPFile[] ftpFiles) {

        if (ftpFiles != null && ftpFiles.length > 0) {
            //loop thru files
            for (FTPFile file : ftpFiles) {
                if (file.isFile()) {
                    System.out.println("File is " + file.getName());
                    fileName = file.getName();
                } else if (file.isDirectory()){
                    System.out.println("Directory is " + file.getName());
                }
            }
        }




    }


    public void handlePrintLogs(ActionEvent actionEvent) {

        job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(null))
        {
            job.printPage(txtLogs);
            job.endJob();
        }

    }

    public void handleSaveConfig(ActionEvent actionEvent) throws IOException {
        String authUsers = "";
        for (int i = 0; i<options.size();i++){
            authUsers += (String) options.get(i)+"^";
        }


        //config = "Normal"+"?" +  txthConfig.getText()  + "#" + txtAuthUsers.getText()+"%";
        try {
            String sql = "UPDATE users SET PreferredConfig=? WHERE ID= '" + owner + "';";
            pst = connection.prepareStatement(sql);
            pst.setString(1, dallas+nfcCard);
            pst.execute();
            pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }




    }

    public void handleChangePass(ActionEvent actionEvent) {

        FXMLLoader Loader = new FXMLLoader();
        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.initStyle(StageStyle.UNDECORATED);

        Loader.setLocation(getClass().getResource("acesPassword.fxml"));
        try{
            Loader.load();
            PasswordController passController = Loader.getController();
            passController.value();
            Parent p = Loader.getRoot();

            window.setScene(new Scene(p));

            window.showAndWait();
        } catch(IOException ex){
            System.out.println("Error loading fxml file");
        }





    }


    public void handleLogout(ActionEvent actionEvent) {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("acesLogin.fxml"));
        try {
            Loader.load();
            Parent p = Loader.getRoot();
            Main.window.setScene(new Scene(p));
            owner = "";

        } catch (IOException e) {
            System.out.println("Error!");
        }
    }
}
