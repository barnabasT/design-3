package com.aces;


import com.connection.ConnectionClass;
import com.socketfx.FxSocketClient;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import org.apache.commons.codec.digest.DigestUtils;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;


public class Controller implements Initializable, ControlledScreen {
    @FXML
    private RadioButton radioAdmin;
    @FXML
    private RadioButton radioNormal;
    @FXML
    private Button btnLogin;
    @FXML
    private Label lblError;
    @FXML
    private TextField txtSNumber;

    @FXML
    private PasswordField txtPassword;

    @FXML
    private Button btnConnect;

    public static FxSocketClient socket;

    public static String logs = "";
    private int con[] = {0,0,0};

    public static String Password;
    public static String AuthKey;
    public static String Owner;
    private boolean connected;

    ScreensController myController;
    public PreparedStatement pst;
    public ResultSet rs;
    private boolean ownerFlag = false;

    ConnectionClass connectionClass = new ConnectionClass();
    Connection connection = connectionClass.getConnection();
    //HTTP STUFF
    private static String GET_URL ;
    private static final String USER_AGENT = "Mozilla/5.0";



    public void sendGET(String GET_URL) throws IOException {
        URL obj = new URL(GET_URL);
        HttpURLConnection httpcon = (HttpURLConnection) obj.openConnection();
        httpcon.setRequestMethod("GET");
        httpcon.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = httpcon.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpcon.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);

                /*if (response.toString().equals("Config")){
                    ownerFlag=false;
                    System.out.println("Config Mode");
                    break;

                }else*/ if(response.toString().equals("Owner")){
                    ownerFlag=true;
                    System.out.println("Owner Mode");
                    break;
                }
                if(!ownerFlag){
                    ownerFlag=false;
                    switch(response.toString()){
                        case "Config,00" : con[0]=0;con[1]=0;break;
                        case "Config,01" : con[0]=0;con[1]=1;break;
                        case "Config,10" : con[0]=1;con[1]=0;break;
                        case "Config,11" : con[0]=1;con[1]=1;break;
                    }
                }else{
                    txtSNumber.setText(response.toString());
                }

            }
            in.close();

            // print result
            System.out.println(response.toString());
            btnLogin.setDisable(false);
            txtSNumber.setDisable(false);
            txtPassword.setDisable(false);
            radioAdmin.setDisable(false);
            radioNormal.setDisable(false);
            btnConnect.setDisable(true);
            btnConnect.setStyle("-fx-background-color:green;");

        } else {
            System.out.println("GET request not worked");
        }

    }




    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }


    public void handleLoginButton(ActionEvent actionEvent) {



        try {
            String sql;
            Owner = txtSNumber.getText();

            if (radioAdmin.isSelected()) {
                AuthKey = DigestUtils.sha256Hex(txtPassword.getText());
                sql = "SELECT * FROM admins WHERE Number = '" + Owner + "' AND AuthKey = '" + AuthKey + "';";

            } else {
                Password = DigestUtils.sha256Hex(txtPassword.getText());
                sql = "SELECT * FROM users WHERE ID = '" + Owner+ "' AND Password = '" + Password + "';";
            }


            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {

                System.out.println("Owner: " + Owner);
                if (radioNormal.isSelected()){
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("acesMain.fxml"));
                    try {
                        Loader.load();
                    } catch (IOException e) {

                    }


                    MainController mainController = Loader.getController();
                    mainController.populateList(Owner);


                    setToggleBtn(mainController);
                    Parent p = Loader.getRoot();
                    Main.window.setScene(new Scene(p));

                }else{
                    FXMLLoader Loader = new FXMLLoader();
                    Loader.setLocation(getClass().getResource("acesAdmin.fxml"));
                    try {
                        Loader.load();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    AdminController adminController = Loader.getController();


                    Parent p = Loader.getRoot();
                    Main.window.setScene(new Scene(p));

                }


            } else {
                lblError.setTextFill(Color.TOMATO);
                lblError.setText("Incorrect credentials. Try again");
                txtSNumber.setText("");
                txtPassword.setText("");
            }
            rs.close();
            pst.close();


        } catch (SQLException e) {
            System.out.println("error in SQL");
            e.printStackTrace();
        }
    }

    private void setToggleBtn(MainController mainController) {

        try {
            System.out.println(Owner);
            String sql = "SELECT PreferredConfig FROM users WHERE ID = '" + Owner + "';";
            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                String config = rs.getString("PreferredConfig");
                System.out.println(config);
                switch(config){
                    case "00" : con[0]=0;con[1]=0;break;
                    case "01" : con[0]=0;con[1]=1;break;
                    case "10" : con[0]=1;con[1]=0;break;
                    case "11" : con[0]=1;con[1]=1;break;
                }
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }


        if (con[0]==1) {
            mainController.btnDallas.setSelected(true);
            mainController.btnDallas.setStyle("-fx-base:  #232930;-fx-text-fill: white;");
            mainController.dallas = "1";
        }else{
            mainController.btnDallas.setSelected(false);
            mainController.dallas = "0";
        }
        if (con[1]==1) {
            mainController.btnNFC.setSelected(true);
            mainController.btnNFC.setStyle("-fx-base:  #232930;-fx-text-fill: white;");
            mainController.nfcCard = "1";
        }else{
            mainController.btnNFC.setSelected(false);
            mainController.nfcCard = "0";
        }


    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //setIsConnected(false);
    }

    public void handleConnectButton(ActionEvent actionEvent) throws IOException {
        System.out.println("Attempting Connection");
        //establishConnection();
        sendGET("http://192.168.4.1/connect");


    }

    public void handleLinkAdmin(ActionEvent actionEvent) {
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            if (desktop.isSupported(Desktop.Action.MAIL)) {
                URI mailto;
                try {
                    mailto = new URI("mailto:216056736@stu.ukzn.ac.za?subject=Need%20Assistance");
                    desktop.mail(mailto);
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void handleRadioAdmin(ActionEvent actionEvent) {
        radioNormal.setSelected(false);

    }

    public void handleRadioNormal(ActionEvent actionEvent) {
        radioAdmin.setSelected(false);
    }






}
