package com.aces;

import javafx.beans.property.SimpleStringProperty;

public class Hub {


    private final SimpleStringProperty ID;
    private final SimpleStringProperty AccessLevel;
    private final SimpleStringProperty Users;
    private final SimpleStringProperty Config;
    private final SimpleStringProperty LastAccess;
    private final SimpleStringProperty Room;
    private final SimpleStringProperty Type;
    private final SimpleStringProperty Owner;
    private final SimpleStringProperty ForceUpdate;


    public Hub(String id, String accessLevel, String users, String config, String lastAccess, String room, String type, String owner, String forceUpdate) {
        this.ID = new SimpleStringProperty(id);
        this.LastAccess = new SimpleStringProperty(lastAccess);
        this.Users = new SimpleStringProperty(users);
        this.Config = new SimpleStringProperty(config);
        this.Room = new SimpleStringProperty(room);
        this.AccessLevel = new SimpleStringProperty(accessLevel);
        this.Type = new SimpleStringProperty(type);
        this.Owner = new SimpleStringProperty(owner);
        this.ForceUpdate = new SimpleStringProperty(forceUpdate);
    }



    public String getID() {
        return ID.get();
    }
    public void setID(String ID) {
        this.ID.set(ID);
    }

    public String getLastAccess() {
        return LastAccess.get();
    }
    public void setLastAccess(String lastAccess) {
        this.LastAccess.set(lastAccess);
    }

    public String getUsers() {
        return Users.get();
    }
    public void setUsers(String users) {
        this.Users.set(users);
    }

    public String getConfig() {return Config.get();}
    public void setConfig(String config) {
        this.Config.set(config);
    }

    public String getRoom() {
        return Room.get();
    }
    public void setRoom(String room) {
        this.Room.set(room);
    }

    public String getAccessLevel() {
        return AccessLevel.get();
    }
    public void setAccessLevel(String accessLevel) {
        this.AccessLevel.set(accessLevel);
    }

    public String getType() {
        return Type.get();
    }
    public void setType(String type) {
        this.Type.set(type);
    }

    public String getOwner() {
        return Owner.get();
    }
    public void setOwner(String owner) {
        this.LastAccess.set(owner);
    }


}
