package com.aces;

import javafx.beans.property.SimpleStringProperty;

public class User {

    private final SimpleStringProperty Password;
    private final SimpleStringProperty PreferredConfig;
    private final SimpleStringProperty ID;
    private final SimpleStringProperty Name;
    private final SimpleStringProperty DallasKeyID;
    private final SimpleStringProperty AccessLevel;

    public User(String id, String name, String dallasKey, String aLevel, String pwd, String prefConfig) {
        this.ID = new SimpleStringProperty(id);
        this.Name = new SimpleStringProperty(name);
        this.Password = new SimpleStringProperty(pwd);
        this.PreferredConfig = new SimpleStringProperty(prefConfig);
        this.DallasKeyID = new SimpleStringProperty(dallasKey);
        this.AccessLevel = new SimpleStringProperty(aLevel);
    }

    public String getPassword() {
        return Password.get();
    }



    public void setPassword(String password) {
        this.Password.set(password);
    }

    public String getPreferredConfig() {
        return PreferredConfig.get();
    }



    public void setPreferredConfig(String preferredConfig) {
        this.PreferredConfig.set(preferredConfig);
    }

    public String getID() {
        return ID.get();
    }



    public void setID(String ID) {
        this.ID.set(ID);
    }

    public String getName() {
        return Name.get();
    }

    public void setName(String name) {
        this.Name.set(name);
    }

    public String getDallasKeyID() {
        return DallasKeyID.get();
    }


    public void setDallasKeyID(String dallasKeyID) {
        this.DallasKeyID.set(dallasKeyID);
    }

    public String getAccessLevel() {
        return AccessLevel.get();
    }



    public void setAccessLevel(String accessLevel) {
        this.AccessLevel.set(accessLevel);
    }




}
