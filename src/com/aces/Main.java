package com.aces;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;




public class Main extends Application {

    public static Stage window;
    @Override
    public void start(Stage primaryStage) throws Exception{
      window = primaryStage;
      //window.initStyle(StageStyle.UNDECORATED);
        Parent root = FXMLLoader.load(getClass().getResource("../Views/acesLogin.fxml"));
        window.setTitle("ACES Access Control");
        window.setOnCloseRequest(e -> {
            e.consume();
            closeProgram();
        });
        window.setScene(new Scene(root, 573, 457));
        window.setResizable(false);
        window.show();
    }
    public static void closeProgram() {
        System.exit(0);
        window.close();
    }

    public static void main(String[] args) {
        launch(args);

    }
}
