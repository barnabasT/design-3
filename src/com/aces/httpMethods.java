package com.aces;

import javafx.scene.control.Alert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class httpMethods {
    private static final String USER_AGENT = "Mozilla/5.0";
    public static String GET_URL;

    public String sendGET(String GET_URL) throws IOException {
        URL obj = new URL(GET_URL);
        HttpURLConnection httpcon = (HttpURLConnection) obj.openConnection();
        httpcon.setRequestMethod("GET");
        httpcon.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = httpcon.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpcon.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response.toString();
            }
        else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setContentText(null);
            alert.setTitle("Connection Error");
            alert.setContentText("Connection was not establish. Try again");
            alert.showAndWait();
            return "error";
        }

    }
}
