package com.aces;

import com.connection.ConnectionClass;
import javafx.beans.binding.DoubleBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.stage.Stage;
import org.apache.commons.codec.digest.DigestUtils;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordController implements Initializable {


    final double max = 6;

    @FXML
    private ProgressBar progress;

    @FXML
    private PasswordField txtPassCurr;
    @FXML
    private PasswordField txtPassNew;
    @FXML
    private PasswordField txtPassConfirm;
    @FXML

    private Button btnCancel;
    @FXML
    private Button btnPasswordChange;

    Controller c = new Controller();

    ConnectionClass connectionClass = new ConnectionClass();
    Connection connection = connectionClass.getConnection();

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void value(){
        DoubleBinding percentage = new DoubleBinding() {
            {
                super.bind(txtPassNew.textProperty());
            }
            @Override
            protected double computeValue() {
                return txtPassNew.getText().length() / max;
            }
        };
        progress.progressProperty().bind(percentage);
    }

    public void handlePasswordChange(ActionEvent actionEvent) {
        //check if password contains the pattern
        Pattern p = Pattern.compile("(((?=.*\\d))(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,15})");
        Matcher m = p.matcher(txtPassNew.getText());

        Boolean newMatch = (txtPassNew.getText().equals(txtPassConfirm.getText())) ? true : false;
        Boolean currMatch = (Controller.Password.equals(DigestUtils.sha256Hex(txtPassCurr.getText()))) ? true : false;
        if(txtPassCurr.getText().isEmpty() | txtPassNew.getText().isEmpty() | txtPassConfirm.getText().isEmpty()){
            messagebox(Alert.AlertType.WARNING,"Error","Fill in all the fields");
        }else {
            validate(m, newMatch,currMatch);
        }



    }

    public void validate(Matcher m, Boolean newMatch, Boolean currMatch){
        if (m.find() && newMatch && currMatch ) {
            messagebox(Alert.AlertType.CONFIRMATION,"Confirmation", "Password Change successful");
            updateDB(txtPassNew.getText());

            Stage stage= (Stage) btnPasswordChange.getScene().getWindow();
            stage.close();

        }else if (!currMatch){
            messagebox(Alert.AlertType.WARNING,"Error","The password entered does not match the current password");
        }else if (!newMatch){
            messagebox(Alert.AlertType.WARNING,"Error","Can not confirm new password. Passwords do not match");
        }else
        {
            messagebox(Alert.AlertType.WARNING,"Error","The password must contain:\nAt least one digit 0-9\n" +
                    "At least one lowercase character\n" +
                    "At least one uppercase character\n" +
                    "At least one special symbol i.e \"@#$%\"");

        }
    }

    private void updateDB(String newPassword) {
        try {
            String sql = "UPDATE users SET Password =? WHERE ID='" + Controller.Owner + "';";
            c.pst = connection.prepareStatement(sql);
            c.pst.setString(1, DigestUtils.sha256Hex(newPassword));
            c.pst.execute();
            c.pst.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void messagebox(Alert.AlertType alertType, String title, String context){
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(context);
        alert.showAndWait();
    }

    public void handleCancel(ActionEvent actionEvent) {
        Stage stage= (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }
}
