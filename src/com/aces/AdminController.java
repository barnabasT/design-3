package com.aces;

import com.connection.ConnectionClass;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.commons.codec.digest.DigestUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class AdminController {

    Controller c = new Controller();

    ConnectionClass connectionClass = new ConnectionClass();
    Connection connection = connectionClass.getConnection();
    final ObservableList userList = FXCollections.observableArrayList();
    final ObservableList hubList = FXCollections.observableArrayList();
    final ObservableList roomList = FXCollections.observableArrayList();
    private String selected;
    private static final String USER_AGENT = "Mozilla/5.0";

    //HUBS
    final ObservableList<Hub> hubData = FXCollections.observableArrayList();

    @FXML
    public TextField txtHubAccess;
    @FXML
    public TextField txtAuthUsers;
    @FXML
    public TextField txthConfig;
    @FXML
    public TextField txtLastAcc;
    @FXML
    public TextField txtRoom;
    @FXML
    public TextField txtType;
    @FXML
    public TextField txtOwner;
    @FXML
    private Tab tabHubs;


    @FXML
    private TableView <Hub> tblHubs;
    @FXML
    private TableColumn<User, String> columnHID;
    @FXML
    private TableColumn<User, String> columnHAccessLevel;
    @FXML
    private TableColumn<User, String> columnAuthUser;
    @FXML
    private TableColumn<User, String> columnConfig;
    @FXML
    private TableColumn<User, String> columnLastAccess;
    @FXML
    private TableColumn<User, String> columnRoom;
    @FXML
    private TableColumn<User, String> columnType;
    @FXML
    private TableColumn<User, String> columnOwner;
    @FXML
    private TableColumn<User, String> columnForceUpdate;

    //USERS
    final ObservableList<User> userData = FXCollections.observableArrayList();

    @FXML
    private ComboBox comboUsers;
    @FXML
    private ComboBox comboHubs;
    @FXML
    private ComboBox comboRoom;

    @FXML
    public TextField txtName;
    @FXML
    public TextField txtDallas;
    @FXML
    public TextField txtAccess;
    @FXML
    public TextField txtPwd;
    @FXML
    public TextField txtConfig;
    @FXML
    private Tab tabUsers;





    @FXML
    private TableView <User> tblUsers;
    @FXML
    private TableColumn<User, String> columnID;
    @FXML
    private TableColumn<User, String> columnName;
    @FXML
    private TableColumn<User, String> columnPreferredConfig;
    @FXML
    private TableColumn<User, String> columnDallasKeyID;
    @FXML
    private TableColumn<User, String> columnAccessLevel;
    @FXML
    private TableColumn<User, String> columnPassword;

    public PreparedStatement pst;
    public ResultSet rs;

    public void reconnect(){
        connectionClass = new ConnectionClass();
        connection = connectionClass.getConnection();
    }



    private void populateHTable() {
        hubData.clear();
        hubList.clear();
        try {
            String sql = "select * from hubs";

            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                hubData.add(new Hub(
                        rs.getString("ID"),
                        rs.getString("AccessLevel"),
                        rs.getString("Users"),
                        rs.getString("Config"),
                        rs.getString("LastAccess"),
                        rs.getString("Room"),
                        rs.getString("Type"),
                        rs.getString("Owner"),
                        rs.getString("ForceUpdate")
                ));
                System.out.println("Hub Data" + hubData);

                columnHID.setCellValueFactory(new PropertyValueFactory<>("ID"));
                columnHAccessLevel.setCellValueFactory(new PropertyValueFactory<>("AccessLevel"));
                columnAuthUser.setCellValueFactory(new PropertyValueFactory<>("Users"));
                columnConfig.setCellValueFactory(new PropertyValueFactory<>("Config"));
                columnLastAccess.setCellValueFactory(new PropertyValueFactory<>("LastAccess"));
                columnRoom.setCellValueFactory(new PropertyValueFactory<>("Room"));
                columnType.setCellValueFactory(new PropertyValueFactory<>("Type"));
                columnOwner.setCellValueFactory(new PropertyValueFactory<>("Owner"));
                columnForceUpdate.setCellValueFactory(new PropertyValueFactory<>("ForceUpdate"));
                tblHubs.setItems(null);
                tblHubs.setItems(hubData);
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            System.out.println("error in SQL");
            e.printStackTrace();
        }
    }



    private void populateUTable() {
        userData.clear();
        userList.clear();
        try {
            String sql = "select * from users";

            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                userData.add(new User(
                        rs.getString("ID"),
                        rs.getString("Name"),
                        rs.getString("DallasKeyID"),
                        rs.getString("AccessLevel"),
                        rs.getString("Password"),
                        rs.getString("PreferredConfig")
                ));
                System.out.println(userData);

                columnID.setCellValueFactory(new PropertyValueFactory<>("ID"));
                columnName.setCellValueFactory(new PropertyValueFactory<>("Name"));
                columnDallasKeyID.setCellValueFactory(new PropertyValueFactory<>("DallasKeyID"));
                columnAccessLevel.setCellValueFactory(new PropertyValueFactory<>("AccessLevel"));
                columnPassword.setCellValueFactory(new PropertyValueFactory<>("Password"));
                columnPreferredConfig.setCellValueFactory(new PropertyValueFactory<>("PreferredConfig"));
                tblUsers.setItems(null);
                tblUsers.setItems(userData);
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            System.out.println("error in SQL");
            e.printStackTrace();
        }
    }

    public ObservableList<User> getUserData()
    {
        return userData;
    }

    public void handleLoadUsers(ActionEvent actionEvent) {
        populateUTable();
        populateCombo();


    }

    public void exit(ActionEvent actionEvent) {
        Main.closeProgram();
    }

    public void handleDelUser(ActionEvent actionEvent) {
        delRow();
    }

    private void delRow(){
        String table="";
        if (tabUsers.isSelected()){
            selected = (String)comboUsers.getSelectionModel().getSelectedItem();
            table = "users";
        }else if(tabHubs.isSelected()){
            selected = (String)comboHubs.getSelectionModel().getSelectedItem();
            selected = (selected==null) ? "": selected;
            table = "hubs";
        }

        System.out.println(selected + " is selected for deletion");
        String sql = "DELETE FROM " + table + " WHERE ID = "+ selected;

        try {
            //String sql = "DELETE FROM ? WHERE ID = '"+ selected +"';";
            System.out.println(sql);
            pst = connection.prepareStatement(sql);
            pst.executeUpdate();


            pst.close();
            if (tabUsers.isSelected()){
                userList.remove(selected);
                populateUTable();
            }else if(tabHubs.isSelected()){
                hubList.remove(selected);
                populateHTable();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        populateCombo();
        clearTextBoxes();
    }

    public void handleAddUser(ActionEvent actionEvent) {
        try {
            String sql = "INSERT INTO users (ID, Name, DallasKeyID, AccessLevel, Password, PreferredConfig) VALUES (?,?,?,?,?,?)";
            System.out.println(sql);
            pst = connection.prepareStatement(sql);

            TextInputDialog dialog = new TextInputDialog("000000");
            dialog.setTitle("Final Step");
            dialog.setHeaderText(null);
            dialog.setContentText("Enter New User's ID:");

            //addRow(dialog, txtName, txtDallas, txtAccess);
            Optional<String> result = dialog.showAndWait();
            String hubID="";

            if(result.isPresent()){
                hubID = result.get();
            }


            pst.setString(1, hubID);
            pst.setString(2, (txtName.getText()==null) ? "" : txtName.getText());
            pst.setString(3, txtDallas.getText());
            pst.setString(4, txtAccess.getText());
            pst.setString(5, DigestUtils.sha256Hex(txtPwd.getText()));
            pst.setString(6,txtConfig.getText());

            alertAdd("User was successfully added");
            pst.execute();
            pst.close();
            populateUTable();
            populateCombo();
            clearTextBoxes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void handleAddHub(ActionEvent actionEvent) {
        try {
            String sql = "INSERT INTO hubs ( AccessLevel, Users, Config, LastAccess, Room, Type, Owner, ForceUpdate) VALUES (?,?,?,?,?,?,?,?)";
            System.out.println(sql);
            pst = connection.prepareStatement(sql);

            /*TextInputDialog dialog = new TextInputDialog("");
            dialog.setTitle("Final Step");
            dialog.setHeaderText(null);
            dialog.setContentText("Enter New Hub ID:");

            //addRow(dialog, txtHubAccess, txtAuthUsers, txthConfig);
            Optional<String> result = dialog.showAndWait();
            String hubID="";

            if(result.isPresent()){
                hubID = result.get();
            }*/


            //pst.setString(1, "");
            pst.setString(1, (txtHubAccess.getText()==null) ? "" : txtHubAccess.getText());
            pst.setString(2, txtAuthUsers.getText());
            pst.setString(3, txthConfig.getText());
            pst.setString(4,txtLastAcc.getText());
            pst.setString(5,txtRoom.getText());
            pst.setString(6,txtType.getText());
            pst.setString(7,txtOwner.getText());
            pst.setString(8,"0");

//            alertAdd();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Success");
            alert.setHeaderText(null);
            alert.setContentText("Hub was successfully added");
            alert.showAndWait();
            pst.execute();
            pst.close();
            populateHTable();
            populateCombo();
            clearTextBoxes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void alertAdd(String s) throws SQLException {
    }


    public void handleDelHub(ActionEvent actionEvent) {
        delRow();
    }

    public void populateCombo(){
        String sql = "";
        if (tabUsers.isSelected()){
            sql = "SELECT * FROM users";
            comboUsers.setItems(userList);
        }else if(tabHubs.isSelected()){
            sql = "SELECT * FROM hubs";
            comboHubs.setItems(hubList);
        }

        //ID POPULATE THE COMBO BOX WITH ID'S
        try {
            System.out.println(sql);
            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                String IDs = rs.getString("ID");
                System.out.println("Hub ID:" + IDs);
                if (tabUsers.isSelected()){
                    userList.add(IDs);
                }else if(tabHubs.isSelected()){
                    hubList.add(IDs);
                }
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        //ROOM POPULATE THE COMBO BOX WITH ROOMS
        /*try {
            System.out.println(sql);
            pst = connection.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                String Rooms = rs.getString("Room");
                System.out.println("Hub ID:" + Rooms);
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }*/


    }

    public void handleComboUsers(ActionEvent actionEvent) {
        try {
            commonCombo(comboUsers, "SELECT * FROM users WHERE ID = ?");

            while (rs.next()) {
                txtName.setText(rs.getString("Name"));
                txtPwd.setText(rs.getString("Password"));
                txtConfig.setText(rs.getString("PreferredConfig"));
                txtAccess.setText(rs.getString("AccessLevel"));
                txtDallas.setText(rs.getString("DallasKeyID"));

            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void handleClearFields(ActionEvent actionEvent) {
        clearTextBoxes();
    }

    public void clearTextBoxes(){
        if(tabUsers.isSelected()){
            System.out.println("Clear User");
            comboUsers.getSelectionModel().clearSelection();
            txtName.setText("");
            txtPwd.setText("");
            txtConfig.setText("");
            txtAccess.setText("");
            txtDallas.setText("");
        }else if(tabHubs.isSelected()){
            System.out.println("Clear Hub");
            comboHubs.getSelectionModel().clearSelection();
            txtHubAccess.setText("");
            txtAuthUsers.setText("");
            txthConfig.setText("");
            txtLastAcc.setText("");
            txtRoom.setText("");
            txtType.setText("");
            txtOwner.setText("");
        }

    }

    public void sendGET(String GET_URL) throws IOException {
        URL obj = new URL(GET_URL);
        HttpURLConnection httpcon = (HttpURLConnection) obj.openConnection();
        httpcon.setRequestMethod("GET");
        httpcon.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = httpcon.getResponseCode();
        System.out.println("GET Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    httpcon.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);

            }
            in.close();

            // print result
            System.out.println(response.toString());

        } else {
            System.out.println("GET request not worked");
        }

    }

    public void handleExport(ActionEvent actionEvent) throws IOException {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Connect to the ACESSetup Access Point");
        alert.setHeaderText(null);
        alert.setTitle("Information");
        alert.showAndWait();

        System.out.println(Controller.AuthKey);
        sendGET("http://192.168.4.1/Config?AuthKey="+Controller.AuthKey);

        exportData();

    }

    private void exportData() throws IOException {

        //if (comboUsers.getSelectionModel().getSelectedItem() != null || comboHubs.getSelectionModel().getSelectedItem() != null ) {
            String config = "";
            if (tabUsers.isSelected()) {
                selected = (String) comboUsers.getSelectionModel().getSelectedItem();
                config = "AdminUser" + "?" + txtConfig.getText() + "$";

            } else if (tabHubs.isSelected()) {
                selected = (String) comboHubs.getSelectionModel().getSelectedItem();
                    config = "Admin" + "?" + "000000" + "*" + Controller.AuthKey + "~" + txtAuthUsers.getText() + "." + txtType.getText() + "," + txtRoom.getText()
                            + "`" + ((txtHubAccess.getText()==null) ? "" :txtHubAccess.getText())  + "@" +  ((txthConfig.getText()==null) ? "" :txthConfig.getText())
                        + "$" + ((txtOwner.getText()==null) ? "" :txtOwner.getText()) + "%";
            }
            System.out.println(config);
            System.out.println("SAVING CONFIGURATIONS:");
            URL obj = new URL("http://192.168.4.1/Config?Hub="+config);
            HttpURLConnection httpcon = (HttpURLConnection) obj.openConnection();
            httpcon.setRequestMethod("GET");
            httpcon.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = httpcon.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);

        /*}else{
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Select ID first");
            alert.showAndWait();
        }*/
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Connect back to the ShortCircuits WiFi then Option-Reconnect");
        alert.setHeaderText(null);
        alert.showAndWait();
        alert.setTitle("Information");

    }

    public void handleComboHubs(ActionEvent actionEvent) {
        try {
            commonCombo(comboHubs, "SELECT * FROM hubs WHERE ID = ?");

            while (rs.next()) {
                txtHubAccess.setText(rs.getString("AccessLevel"));
                txtAuthUsers.setText(rs.getString("Users"));
                txthConfig.setText(rs.getString("Config"));
                txtLastAcc.setText(rs.getString("LastAccess"));
                txtRoom.setText(rs.getString("Room"));
                txtType.setText(rs.getString("Type"));
                txtOwner.setText(rs.getString("Owner"));
            }
            rs.close();
            pst.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void commonCombo(ComboBox comboHubs, String s) throws SQLException {
        selected = (String) comboHubs.getSelectionModel().getSelectedItem();
        System.out.println(selected);
        String sql = s;
        System.out.println(sql);
        pst = connection.prepareStatement(sql);
        pst.setString(1,selected);
        rs = pst.executeQuery();
    }

    public void handleUpdateUsers(ActionEvent actionEvent) {
        selected = (String)comboUsers.getSelectionModel().getSelectedItem();

        try {
            String sql = "UPDATE users SET Name=?, DallasKeyID=?, AccessLevel=?, Password=?, PreferredConfig=? WHERE ID= '" + selected + "';";
            System.out.println(sql);
            pst = connection.prepareStatement(sql);
            pst.setString(1, txtName.getText());
            pst.setString(2, txtDallas.getText());
            pst.setString(3, txtAccess.getText());
            pst.setString(4, DigestUtils.sha256Hex(txtPwd.getText()));
            pst.setString(5,txtConfig.getText());

            pst.executeUpdate();
            pst.close();
            populateUTable();
            populateCombo();
            clearTextBoxes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void handleUpdateHubs(ActionEvent actionEvent) {
        selected = (String)comboHubs.getSelectionModel().getSelectedItem();

        try {
            String sql = "UPDATE hubs SET  AccessLevel=?, Users=?, Config=?, LastAccess=?, Room=?, Type=?, Owner=? WHERE ID= '" + selected + "';";
            System.out.println(sql);
            pst = connection.prepareStatement(sql);
            pst.setString(1, txtHubAccess.getText());
            pst.setString(2, txtAuthUsers.getText());
            pst.setString(3, txthConfig.getText());
            pst.setString(4,txtLastAcc.getText());
            pst.setString(5,txtRoom.getText());
            pst.setString(6,txtType.getText());
            pst.setString(7,txtOwner.getText());

            pst.executeUpdate();
            pst.close();
            populateHTable();
            populateCombo();

            clearTextBoxes();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void handleLoadHubs(ActionEvent actionEvent) {
        populateHTable();
        populateCombo();
    }

    public void handleReconnect(ActionEvent actionEvent) {
        reconnect();
    }

    public void handleComboRoom(Event event) {
    }

    public void handleLogout(ActionEvent actionEvent) {
        FXMLLoader Loader = new FXMLLoader();
        Loader.setLocation(getClass().getResource("acesLogin.fxml"));
        try {
            Loader.load();
            Parent p = Loader.getRoot();
            Main.window.setScene(new Scene(p));

        } catch (IOException e) {
            System.out.println("Error!");
        }


    }
}
